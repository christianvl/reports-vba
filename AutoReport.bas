'Christian Fairlie Pearson van Langendonck
'Algonquin College - COOP Career Leader
'February 2021 (last rev. 2021.02.11)
'Macro to automate Students Report formatting
'Reports must have all the default columns, active students, and include counts.

'This script will:
'- Read column headers and remove unnecessary columns
'- Preserve the headings and exclude them from the following steps;
'- Clear default formatting colors;
'- Apply "text to columns" to columns with only numbers (most notably "counts");
'- Re-order columns and insert filters in the heading.
'- Highlight rows in Students and Jobs tables
'- Create summary table with highlighted rows controls and results.

'BEGIN macro
Sub AutoReport()

    'Declare global worksheet variables
    Dim studentsSheet As Worksheet
    Dim employersSheet As Worksheet
    Dim summarySheet As Worksheet
    'Set the working sheets
    Set studentsSheet = Sheet1
    Set employersSheet = Sheet2
    Set summarySheet = Sheet3

    'List of columns to keep in the report
    'CHANGES TO THIS LIST MUST BE CAREFUL
    'CHANGES TO COLUMNS POSITION AND QUANTITY MAY CAUSE UNEXPECTED RESULTS
    'Adding or removing items here may require further changes - check all variables with the suffix ColumnIndex
    listOfStudentsColumns = Array("Student ID", "First Name", "Last Name", "Term Label", "Released", "Employed", "Primary Email", "Organization", "Division", "Program Name", "Total View Count", "Application Count", "Selected for Interview Count")
    listOfJobsColumns = Array("Job ID", "Name", "Division Name", "Position", "Posting Status", "Internal Status", "Application Deadline", "City", "Application Delivery Method", "Security Clearance", "Source of job posting", "Number of Positions", "Total View Count", "Unique View Count", "Application Count", "Selected for Interview Count")

    'BEGIN - Remove unnecessary columns
    'Check if the columns were already removed (so, only perform this task in first run)'
    If Not IsEmpty(studentsSheet.Cells(3, 40)) Then
        'Loop Students columns backwards (removing columns will affect forward column count), identify the names and delete columns
        For i = studentsSheet.Cells(2, studentsSheet.Columns.Count).End(xlToLeft).Column To 1 Step -1
            If Not inArray(listOfStudentsColumns, studentsSheet.Cells(2, i).Value) Then
            studentsSheet.Columns(i).EntireColumn.Delete
            End If
        Next

        'Loop Jobs columns backwards (removing columns will affect forward column count), identify the names and delete columns
        For i = employersSheet.Cells(2, employersSheet.Columns.Count).End(xlToLeft).Column To 1 Step -1
            If Not inArray(listOfJobsColumns, employersSheet.Cells(2, i).Value) Then
                employersSheet.Columns(i).EntireColumn.Delete
            End If
        Next

        'Index columns to re-order'
        'Identify "Program Name" column position'
        Dim ProgramNameColumnIndex As Integer
        ProgramNameColumnIndex = Application.WorksheetFunction.Match("Program Name", studentsSheet.Range("2:2"), 0)
        'Identify "Number of positions" column position'
        Dim NumberOfPositionsColumnIndex As Integer
        NumberOfPositionsColumnIndex = Application.WorksheetFunction.Match("Number of Positions", employersSheet.Range("2:2"), 0)
        'Move columns position
        'Jobs'
        'Place Number of applications as last column before counts'
        For i = employersSheet.Cells(2, employersSheet.Columns.Count).End(xlToLeft).Column To 1 Step -1
            If StrComp(employersSheet.Cells(1, i).Value, "Counts") = 0 Then
                employersSheet.Columns(i).Insert XlDirection.xlToLeft
                employersSheet.Columns(i).Value = employersSheet.Columns(NumberOfPositionsColumnIndex).Value
                employersSheet.Columns(NumberOfPositionsColumnIndex).Delete
                Exit For
            End If
        Next
        'Students'
        'Place "Program Name" as last column before counts
        For i = studentsSheet.Cells(2, studentsSheet.Columns.Count).End(xlToLeft).Column To 1 Step -1
            If StrComp(studentsSheet.Cells(1, i).Value, "Counts") = 0 Then
                studentsSheet.Columns(i).Insert XlDirection.xlToLeft
                studentsSheet.Columns(i).Value = studentsSheet.Columns(ProgramNameColumnIndex).Value
                studentsSheet.Columns(ProgramNameColumnIndex).Delete
                Exit For
            End If
        Next
    End If
    'END - Remove unnecessary columns

    'Declare students global variables for last row, column and full table range
    Dim studentsLastRow As Long
    Dim studentsLastColumn As Long
    Dim studentsFullRange As Range
    'Declare jobs global variables for last row, column and full table range
    Dim employersLastRow As Long
    Dim employersLastColumn As Long
    Dim employersFullRange As Range

    'Find last row
    studentsLastRow = studentsSheet.Cells(studentsSheet.Rows.Count, "A").End(xlUp).Row
    employersLastRow = employersSheet.Cells(employersSheet.Rows.Count, "A").End(xlUp).Row
    'Find last Column
    studentsLastColumn = studentsSheet.Cells(3, studentsSheet.Columns.Count).End(xlToLeft).Column
    employersLastColumn = employersSheet.Cells(3, employersSheet.Columns.Count).End(xlToLeft).Column

    'Assign range where -> Range(From Cells(row, column), To Cells(row, column)
    'Exclude heading from actions
    Set studentsFullRange = studentsSheet.Range(studentsSheet.Cells(3, 1), studentsSheet.Cells(studentsLastRow, studentsLastColumn))
    Set employersFullRange = employersSheet.Range(employersSheet.Cells(3, 1), employersSheet.Cells(employersLastRow, employersLastColumn))

    'Declare and assign range for table headers'
    Dim studentsHeader As Range
    Dim employersHeader As Range
    Set studentsHeader = studentsSheet.Range(studentsSheet.Cells(2, 1), studentsSheet.Cells(2, studentsLastColumn))
    Set employersHeader = employersSheet.Range(employersSheet.Cells(2, 1), employersSheet.Cells(2, employersLastColumn))

    'Chamge columns sizes to fit header and content text'
    studentsHeader.Columns.EntireColumn.AutoFit
    employersSheet.Columns.EntireColumn.AutoFit

    'Clear all table formattings
    studentsFullRange.ClearFormats
    employersFullRange.ClearFormats

    'Index all columns
    'Declare integer variables to store index number of columns'
    'Students'
    Dim studentStudentIdColumnIndex As Integer
    studentStudentIdColumnIndex = Application.WorksheetFunction.Match("Student ID", studentsHeader, 0)
    Dim studentFirstNameColumnIndex As Integer
    studentFirstNameColumnIndex = Application.WorksheetFunction.Match("First Name", studentsHeader, 0)
    Dim studentLastNameColumnIndex As Integer
    studentLastNameColumnIndex = Application.WorksheetFunction.Match("Last Name", studentsHeader, 0)
    Dim studentTermLabeldColumnIndex As Integer
    studentTermLabeldColumnIndex = Application.WorksheetFunction.Match("Term Label", studentsHeader, 0)
    Dim studentReleasedColumnIndex As Integer
    studentReleasedColumnIndex = Application.WorksheetFunction.Match("Released", studentsHeader, 0)
    Dim studentEmployedColumnIndex As Integer
    studentEmployedColumnIndex = Application.WorksheetFunction.Match("Employed", studentsHeader, 0)
    Dim studentPrimaryEmailColumnIndex As Integer
    studentPrimaryEmailColumnIndex = Application.WorksheetFunction.Match("Primary Email", studentsHeader, 0)
    Dim studentOrganizationColumnIndex As Integer
    studentOrganizationColumnIndex = Application.WorksheetFunction.Match("Organization", studentsHeader, 0)
    Dim studentDivisionColumnIndex As Integer
    studentDivisionColumnIndex = Application.WorksheetFunction.Match("Division", studentsHeader, 0)
    Dim studentTotalViewCountColumnIndex As Integer
    studentTotalViewCountColumnIndex = Application.WorksheetFunction.Match("Total View Count", studentsHeader, 0)
    Dim studentApplicationCountColumnIndex As Integer
    studentApplicationCountColumnIndex = Application.WorksheetFunction.Match("Application Count", studentsHeader, 0)
    Dim studentSelectIntvCountColumnIndex As Integer
    studentSelectIntvCountColumnIndex = Application.WorksheetFunction.Match("Selected for Interview Count", studentsHeader, 0)
    'Reassign value for previously moved column'
    ProgramNameColumnIndex = Application.WorksheetFunction.Match("Program Name", studentsHeader, 0)
    'Jobs'
    Dim jobsJobIdColumnIndex As Integer
    jobsJobIdColumnIndex = Application.WorksheetFunction.Match("Job ID", employersHeader, 0)
    Dim jobsNameColumnIndex As Integer
    jobsNameColumnIndex = Application.WorksheetFunction.Match("Name", employersHeader, 0)
    Dim jobsDivisionColumnIndex As Integer
    jobsDivisionColumnIndex = Application.WorksheetFunction.Match("Division Name", employersHeader, 0)
    Dim jobsPositionColumnIndex As Integer
    jobsPositionColumnIndex = Application.WorksheetFunction.Match("Position", employersHeader, 0)
    Dim jobsPostingStatusColumnIndex As Integer
    jobsPostingStatusColumnIndex = Application.WorksheetFunction.Match("Posting Status", employersHeader, 0)
    Dim jobsInternalStatusColumnIndex As Integer
    jobsInternalStatusColumnIndex = Application.WorksheetFunction.Match("Internal Status", employersHeader, 0)
    Dim jobsApplicationDeadlineColumnIndex As Integer
    jobsApplicationDeadlineColumnIndex = Application.WorksheetFunction.Match("Application Deadline", employersHeader, 0)
    Dim jobsCityColumnIndex As Integer
    jobsCityColumnIndex = Application.WorksheetFunction.Match("City", employersHeader, 0)
    Dim jobsAppDeliveryMethodColumnIndex As Integer
    jobsAppDeliveryMethodColumnIndex = Application.WorksheetFunction.Match("Application Delivery Method", employersHeader, 0)
    Dim jobsSecurityClearanceColumnIndex As Integer
    jobsSecurityClearanceColumnIndex = Application.WorksheetFunction.Match("Security Clearance", employersHeader, 0)
    Dim jobsSourcePostingColumnIndex As Integer
    jobsSourcePostingColumnIndex = Application.WorksheetFunction.Match("Source of job posting", employersHeader, 0)
    Dim jobsTotalViewCountColumnIndex As Integer
    jobsTotalViewCountColumnIndex = Application.WorksheetFunction.Match("Total View Count", employersHeader, 0)
    Dim jobsUniqueViewCountColumnIndex As Integer
    jobsUniqueViewCountColumnIndex = Application.WorksheetFunction.Match("Unique View Count", employersHeader, 0)
    Dim jobsAppCountColumnIndex As Integer
    jobsAppCountColumnIndex = Application.WorksheetFunction.Match("Application Count", employersHeader, 0)
    Dim jobsSelIntvCountColumnIndex As Integer
    jobsSelIntvCountColumnIndex = Application.WorksheetFunction.Match("Selected for Interview Count", employersHeader, 0)
    'Reassign value for previously moved column'
    NumberOfPositionsColumnIndex = Application.WorksheetFunction.Match("Number of Positions", employersHeader, 0)

    'BEGIN - Text to column (convert columns with numbers, from numbers interpreted as text to numbers)
    'Declare variables for count columns ranges (Students)
    Dim studentsTotalView As Range
    Dim studentsApplications As Range
    Dim studentsInterviews As Range
    Dim studentID As Range
    'Assign the Ranges to each variable (Students)
    Set studentsTotalView = studentsSheet.Range(studentsSheet.Cells(3, studentTotalViewCountColumnIndex), studentsSheet.Cells(studentsLastRow, studentTotalViewCountColumnIndex))
    Set studentsApplications = studentsSheet.Range(studentsSheet.Cells(3, studentApplicationCountColumnIndex), studentsSheet.Cells(studentsLastRow, studentApplicationCountColumnIndex))
    Set studentsInterviews = studentsSheet.Range(studentsSheet.Cells(3, studentsLastColumn), studentsSheet.Cells(studentsLastRow, studentsLastColumn))
    Set studentID = studentsSheet.Range(studentsSheet.Cells(3, studentStudentIdColumnIndex), studentsSheet.Cells(studentsLastRow, studentStudentIdColumnIndex))

    'Declare Range variables for each count column (Employers)
    Dim employersTotalViews As Range
    Dim employersUnique As Range
    Dim employersAppCount As Range
    Dim employersInterviews As Range
    Dim jobID As Range
    'Assign the Ranges to each variable (Employers)
    Set employersTotalViews = employersSheet.Range(employersSheet.Cells(3, jobsTotalViewCountColumnIndex), employersSheet.Cells(employersLastRow, jobsTotalViewCountColumnIndex))
    Set employersUnique = employersSheet.Range(employersSheet.Cells(3, jobsUniqueViewCountColumnIndex), employersSheet.Cells(employersLastRow, jobsUniqueViewCountColumnIndex))
    Set employersAppCount = employersSheet.Range(employersSheet.Cells(3, jobsAppCountColumnIndex), employersSheet.Cells(employersLastRow, jobsAppCountColumnIndex))
    Set employersInterviews = employersSheet.Range(employersSheet.Cells(3, jobsSelIntvCountColumnIndex), employersSheet.Cells(employersLastRow, jobsSelIntvCountColumnIndex))
    Set jobID = employersSheet.Range(employersSheet.Cells(3, jobsJobIdColumnIndex), employersSheet.Cells(employersLastRow, jobsJobIdColumnIndex))

    'Declare variables for employed, released and status ranges
    Dim released As Range
    Dim employed As Range
    Dim status As Range
    Dim positions As Range
    Dim sourceOfPosting As Range
    Dim appDeliveryMethod As Range
    Set released = studentsSheet.Range(studentsSheet.Cells(3, studentReleasedColumnIndex), studentsSheet.Cells(studentsLastRow, studentReleasedColumnIndex))
    Set employed = studentsSheet.Range(studentsSheet.Cells(3, studentEmployedColumnIndex), studentsSheet.Cells(studentsLastRow, studentEmployedColumnIndex))
    Set status = employersSheet.Range(employersSheet.Cells(3, jobsPostingStatusColumnIndex), employersSheet.Cells(employersLastRow, jobsPostingStatusColumnIndex))
    Set positions = employersSheet.Range(employersSheet.Cells(3, NumberOfPositionsColumnIndex), employersSheet.Cells(employersLastRow, NumberOfPositionsColumnIndex))
    Set sourceOfPosting = employersSheet.Range(employersSheet.Cells(3, jobsSourcePostingColumnIndex), employersSheet.Cells(employersLastRow, jobsSourcePostingColumnIndex))
    Set appDeliveryMethod = employersSheet.Range(employersSheet.Cells(3, jobsAppDeliveryMethodColumnIndex), employersSheet.Cells(employersLastRow, jobsAppDeliveryMethodColumnIndex))

    'Apply text to columns individually (Students)
    studentsTotalView.TextToColumns _
          Destination:=studentsTotalView, _
          DataType:=xlDelimited, _
          TextQualifier:=xlDoubleQuote, _
          ConsecutiveDelimiter:=False, Tab:=True, _
          Semicolon:=False, Comma:=False, Space:=False, Other:=False, _
          FieldInfo:=Array(1, 1), TrailingMinusNumbers:=True

    studentsApplications.TextToColumns _
          Destination:=studentsApplications, _
          DataType:=xlDelimited, _
          TextQualifier:=xlDoubleQuote, _
          ConsecutiveDelimiter:=False, Tab:=True, _
          Semicolon:=False, Comma:=False, Space:=False, Other:=False, _
          FieldInfo:=Array(1, 1), TrailingMinusNumbers:=True

    studentsInterviews.TextToColumns _
          Destination:=studentsInterviews, _
          DataType:=xlDelimited, _
          TextQualifier:=xlDoubleQuote, _
          ConsecutiveDelimiter:=False, Tab:=True, _
          Semicolon:=False, Comma:=False, Space:=False, Other:=False, _
          FieldInfo:=Array(1, 1), TrailingMinusNumbers:=True

    studentID.TextToColumns _
          Destination:=studentID, _
          DataType:=xlDelimited, _
          TextQualifier:=xlDoubleQuote, _
          ConsecutiveDelimiter:=False, Tab:=True, _
          Semicolon:=False, Comma:=False, Space:=False, Other:=False, _
          FieldInfo:=Array(1, 1), TrailingMinusNumbers:=True

    'Apply text to columns individually (Jobs)
    employersTotalViews.TextToColumns _
          Destination:=employersTotalViews, _
          DataType:=xlDelimited, _
          TextQualifier:=xlDoubleQuote, _
          ConsecutiveDelimiter:=False, Tab:=True, _
          Semicolon:=False, Comma:=False, Space:=False, Other:=False, _
          FieldInfo:=Array(1, 1), TrailingMinusNumbers:=True

    employersUnique.TextToColumns _
          Destination:=employersUnique, _
          DataType:=xlDelimited, _
          TextQualifier:=xlDoubleQuote, _
          ConsecutiveDelimiter:=False, Tab:=True, _
          Semicolon:=False, Comma:=False, Space:=False, Other:=False, _
          FieldInfo:=Array(1, 1), TrailingMinusNumbers:=True

    employersAppCount.TextToColumns _
          Destination:=employersAppCount, _
          DataType:=xlDelimited, _
          TextQualifier:=xlDoubleQuote, _
          ConsecutiveDelimiter:=False, Tab:=True, _
          Semicolon:=False, Comma:=False, Space:=False, Other:=False, _
          FieldInfo:=Array(1, 1), TrailingMinusNumbers:=True

    employersInterviews.TextToColumns _
          Destination:=employersInterviews, _
          DataType:=xlDelimited, _
          TextQualifier:=xlDoubleQuote, _
          ConsecutiveDelimiter:=False, Tab:=True, _
          Semicolon:=False, Comma:=False, Space:=False, Other:=False, _
          FieldInfo:=Array(1, 1), TrailingMinusNumbers:=True
    'END - Text to column

    'BEGIN - Add filter to table headers
    If Not studentsSheet.AutoFilterMode Then
        studentsHeader.AutoFilter
    End If
    If Not employersSheet.AutoFilterMode Then
        employersHeader.AutoFilter
    End If
    'END - Add filter to table headers

    'BEGIN - Declare variables to calculate percentages and set report criterias
    'Values of control tables in Summary sheet
    Dim totalJobPostings As Integer
    totalJobPostings = WorksheetFunction.Count(jobID)

    Dim ctrEmployed As Variant
    ctrEmployed = summarySheet.Range("C18").Value

    Dim ctrReleased As Variant
    Dim ctrReleasedEmployed As Variant
    ctrReleased = summarySheet.Range("D19").Value
    ctrReleasedEmployed = summarySheet.Range("C19").Value

    Dim ctrViewsEmployed As Variant
    Dim ctrViewsReleased As Variant
    Dim ctrViews As Long
    Dim ctrViewsInterv As Long
    ctrViewsEmployed = summarySheet.Range("C20").Value
    ctrViewsReleased = summarySheet.Range("D20").Value
    ctrViews = summarySheet.Range("E20").Value
    ctrViewsInterv = summarySheet.Range("G20").Value

    Dim ctrStdsNoApp As Long
    Dim ctrStdsNoAppEmployed As Variant
    Dim ctrStdsNoAppReleased As Variant
    ctrStdsNoAppEmployed = summarySheet.Range("C21").Value
    ctrStdsNoAppReleased = summarySheet.Range("D21").Value
    ctrStdsNoApp = summarySheet.Range("F21").Value

    Dim lowApp As Long
    Dim ctrLowAppEmployed As Variant
    Dim ctrLowAppReleased As Variant
    Dim ctrLowAppInterv As Long
    lowApp = summarySheet.Range("I7").Value * (totalJobPostings / 100) 'F22'
    ctrLowAppEmployed = summarySheet.Range("C22").Value
    ctrLowAppReleased = summarySheet.Range("D22").Value
    ctrLowAppInterv = summarySheet.Range("G22").Value

    Dim highApp As Long
    Dim ctrHihghAppEmployed As Variant
    Dim ctrHihghAppReleased As Variant
    Dim ctrHihghAppInterv As Long
    highApp = summarySheet.Range("I8").Value * (totalJobPostings / 100) 'F23'
    ctrHihghAppEmployed = summarySheet.Range("C23").Value
    ctrHihghAppReleased = summarySheet.Range("D23").Value
    ctrHihghAppInterv = summarySheet.Range("G23").Value

    Dim highInterview As Long
    Dim ctrHighIntervEmployed As Variant
    Dim ctrHighIntervReleased As Variant
    highInterview = summarySheet.Range("I9").Value 'G24'
    ctrHighIntervEmployed = summarySheet.Range("C24").Value
    ctrHighIntervReleased = summarySheet.Range("D24").Value

    Dim ctrCancelled As Variant
    Dim ctrCancelledInterv As Integer
    ctrCancelled = summarySheet.Range("E26").Value
    ctrCancelledInterv = summarySheet.Range("G26").Value

    Dim ctrNoInterv As Variant
    Dim ctrNoIntervCount As Long
    ctrNoInterv = summarySheet.Range("E27").Value
    ctrNoIntervCount = summarySheet.Range("G27").Value

    Dim ctrJobsNoApp As Variant
    Dim ctrJobsNoAppCount As Long
    ctrJobsNoApp = summarySheet.Range("E28").Value
    ctrJobsNoAppCount = summarySheet.Range("F28").Value
    'END - Declare variables to calculate percentages and set report criterias

    'BEGIN - Control switch for color highlighting on/off
    'Anything other than "N" will apply colors
    If summarySheet.Range("I10").Value = "N" Then
        studentsFullRange.ClearFormats
        employersFullRange.ClearFormats
    Else
        'BEGIN - Format rows to add colors (fill color) - Students
        'GREEN = employed
        studentsFullRange.FormatConditions.Add Type:=xlExpression, Formula1:="=$" & ColumnLetter(studentEmployedColumnIndex) & "3=" & Chr(34) & ctrEmployed & Chr(34)
        studentsFullRange.FormatConditions(studentsFullRange.FormatConditions.Count).SetFirstPriority
        With studentsFullRange.FormatConditions(1).Interior
            .PatternColorIndex = xlAutomatic
            .ThemeColor = xlThemeColorAccent6
            .TintAndShade = 0
        End With
        studentsFullRange.FormatConditions(1).StopIfTrue = False

        'RED = not released not employed
        studentsFullRange.FormatConditions.Add Type:=xlExpression, Formula1:="=AND($" & ColumnLetter(studentReleasedColumnIndex) & "3=" & Chr(34) & ctrReleased & Chr(34) & ",$" & ColumnLetter(studentEmployedColumnIndex) & "3=" & Chr(34) & ctrReleasedEmployed & Chr(34) & ")"
        studentsFullRange.FormatConditions(studentsFullRange.FormatConditions.Count).SetFirstPriority
        With studentsFullRange.FormatConditions(1).Font
            .Color = -16777024
            .TintAndShade = 0
        End With
        With studentsFullRange.FormatConditions(1).Interior
            .PatternColorIndex = xlAutomatic
            .Color = 10066431
            .TintAndShade = 0
        End With
        studentsFullRange.FormatConditions(1).StopIfTrue = False

        'YELLOW = released, not employed, no views
        studentsFullRange.FormatConditions.Add Type:=xlExpression, Formula1:="=AND($" & ColumnLetter(studentTotalViewCountColumnIndex) & "3=" & ctrViews & ",$" & ColumnLetter(studentReleasedColumnIndex) & "3=" & Chr(34) & ctrViewsReleased & Chr(34) & ",$" & ColumnLetter(studentEmployedColumnIndex) & "3=" & Chr(34) & ctrViewsEmployed & Chr(34) & ")"
        studentsFullRange.FormatConditions(studentsFullRange.FormatConditions.Count).SetFirstPriority
        With studentsFullRange.FormatConditions(1).Interior
            .PatternColorIndex = xlAutomatic
            .ThemeColor = xlThemeColorAccent4
            .TintAndShade = 0.599963377788629
        End With
        studentsFullRange.FormatConditions(1).StopIfTrue = False
        'YELLOW = released, not employed, no applications, views > 0
        studentsFullRange.FormatConditions.Add Type:=xlExpression, Formula1:="=AND($" & ColumnLetter(studentApplicationCountColumnIndex) & "3=" & ctrStdsNoApp & ",$" & ColumnLetter(studentReleasedColumnIndex) & "3=" & Chr(34) & ctrStdsNoAppReleased & Chr(34) & ",$" & ColumnLetter(studentEmployedColumnIndex) & "3=" & Chr(34) & ctrStdsNoAppEmployed & Chr(34) & ",$K3>0)"
        studentsFullRange.FormatConditions(studentsFullRange.FormatConditions.Count).SetFirstPriority
        With studentsFullRange.FormatConditions(1).Interior
            .PatternColorIndex = xlAutomatic
            .ThemeColor = xlThemeColorAccent4
            .TintAndShade = 0.599963377788629
        End With
        studentsFullRange.FormatConditions(1).StopIfTrue = False

        'BLUE = released, not employed, no interviews, applications < % job postings, views > 0
        studentsFullRange.FormatConditions.Add Type:=xlExpression, Formula1:="=AND($" & ColumnLetter(studentApplicationCountColumnIndex) & "3<=" & lowApp & ",$" & ColumnLetter(studentApplicationCountColumnIndex) & "3>0,$" & ColumnLetter(studentReleasedColumnIndex) & "3=" & Chr(34) & ctrLowAppReleased & Chr(34) & ",$" & ColumnLetter(studentEmployedColumnIndex) & "3=" & Chr(34) & ctrLowAppEmployed & Chr(34) & ",$" & ColumnLetter(studentSelectIntvCountColumnIndex) & "3=" & ctrLowAppInterv & ",$" & ColumnLetter(studentTotalViewCountColumnIndex) & "3>0)"
        studentsFullRange.FormatConditions(studentsFullRange.FormatConditions.Count).SetFirstPriority
        With studentsFullRange.FormatConditions(1).Interior
            .PatternColorIndex = xlAutomatic
            .ThemeColor = xlThemeColorAccent5
            .TintAndShade = 0.399945066682943
        End With
        studentsFullRange.FormatConditions(1).StopIfTrue = False

        'ORANGE = released, not employed, no interviews, applications > % job postings
        studentsFullRange.FormatConditions.Add Type:=xlExpression, Formula1:= _
            "=AND($" & ColumnLetter(studentApplicationCountColumnIndex) & "3>" & highApp & ",$" & ColumnLetter(studentReleasedColumnIndex) & "3=" & Chr(34) & ctrHihghAppReleased & Chr(34) & ",$" & ColumnLetter(studentEmployedColumnIndex) & "3=" & Chr(34) & ctrHihghAppEmployed & Chr(34) & ",$" & ColumnLetter(studentSelectIntvCountColumnIndex) & "3= " & ctrHihghAppInterv & ")"
        studentsFullRange.FormatConditions(studentsFullRange.FormatConditions.Count).SetFirstPriority
        With studentsFullRange.FormatConditions(1).Interior
            .PatternColorIndex = xlAutomatic
            .ThemeColor = xlThemeColorAccent2
            .TintAndShade = 0
        End With
        studentsFullRange.FormatConditions(1).StopIfTrue = False
        'ORANGE - released, not employed, interviews > #
        studentsFullRange.FormatConditions.Add Type:=xlExpression, Formula1:= _
            "=AND($" & ColumnLetter(studentSelectIntvCountColumnIndex) & "3>" & highInterview & ",$" & ColumnLetter(studentReleasedColumnIndex) & "3=" & Chr(34) & ctrHighIntervReleased & Chr(34) & ",$" & ColumnLetter(studentEmployedColumnIndex) & "3=" & Chr(34) & ctrHighIntervEmployed & Chr(34) & ")"
        studentsFullRange.FormatConditions(studentsFullRange.FormatConditions.Count).SetFirstPriority
        With studentsFullRange.FormatConditions(1).Interior
            .PatternColorIndex = xlAutomatic
            .ThemeColor = xlThemeColorAccent2
            .TintAndShade = 0
        End With
        studentsFullRange.FormatConditions(1).StopIfTrue = False
        'END - Format rows to add colors (fill color) - Students

        'BEGIN - Format rows to add colors (fill color) - Jobs
        'YELLOW = not cancelled, no interviews, not student developed, through HireAC
        employersFullRange.FormatConditions.Add Type:=xlExpression, Formula1:="=AND($" & ColumnLetter(jobsSelIntvCountColumnIndex) & "3=" & ctrNoIntervCount & ", $" & ColumnLetter(jobsPostingStatusColumnIndex) & "3<>""Cancelled"", $" & ColumnLetter(jobsSourcePostingColumnIndex) & "3<>""Student Developed"", $" & ColumnLetter(jobsAppDeliveryMethodColumnIndex) & "3<>""Through Employer Website"")"
        employersFullRange.FormatConditions(employersFullRange.FormatConditions.Count).SetFirstPriority
        With employersFullRange.FormatConditions(1).Interior
            .PatternColorIndex = xlAutomatic
            .ThemeColor = xlThemeColorAccent4
            .TintAndShade = 0.599963377788629
        End With
        employersFullRange.FormatConditions(1).StopIfTrue = False

        'ORANGE = not cancelled, no applications, not student developed
        employersFullRange.FormatConditions.Add Type:=xlExpression, Formula1:="=AND($" & ColumnLetter(jobsAppCountColumnIndex) & "3=" & ctrJobsNoAppCount & ", $" & ColumnLetter(jobsPostingStatusColumnIndex) & "3<>""Cancelled"", $" & ColumnLetter(jobsSourcePostingColumnIndex) & "3<>""Student Developed"")"
        employersFullRange.FormatConditions(employersFullRange.FormatConditions.Count).SetFirstPriority
        With employersFullRange.FormatConditions(1).Interior
            .PatternColorIndex = xlAutomatic
            .ThemeColor = xlThemeColorAccent2
            .TintAndShade = 0
        End With
        employersFullRange.FormatConditions(1).StopIfTrue = False

        'RED = cancelled and no interviews
        employersFullRange.FormatConditions.Add Type:=xlExpression, Formula1:="=AND($" & ColumnLetter(jobsPostingStatusColumnIndex) & "3=""Cancelled"", $" & ColumnLetter(jobsSelIntvCountColumnIndex) & "3= " & ctrCancelledInterv & ")"
        employersFullRange.FormatConditions(employersFullRange.FormatConditions.Count).SetFirstPriority
        With employersFullRange.FormatConditions(1).Font
            .Color = -16777024
            .TintAndShade = 0
        End With
        With employersFullRange.FormatConditions(1).Interior
            .PatternColorIndex = xlAutomatic
            .Color = 10066431
            .TintAndShade = 0
        End With
        employersFullRange.FormatConditions(1).StopIfTrue = False
        'END - Format rows to add colors (fill color) - Jobs

    End If
    'END - Control switch color highlighting on/off

    'BEGIN - Update summary tables
    'Ref. Students'
    'Employed'
    summarySheet.Range("C7").Value = WorksheetFunction.CountIf(employed, ctrEmployed)
    'Not released
    summarySheet.Range("C8").Value = WorksheetFunction.CountIfs(released, ctrReleased, employed, ctrReleasedEmployed)
    'No views
    summarySheet.Range("C9").Value = WorksheetFunction.CountIfs(released, ctrViewsReleased, employed, ctrViewsEmployed, studentsTotalView, 0)
    'No applications'
    summarySheet.Range("C10").Value = WorksheetFunction.CountIfs(released, ctrStdsNoAppReleased, employed, ctrStdsNoAppEmployed, studentsApplications, 0, studentsTotalView, ">0")
    'Low application'
    summarySheet.Range("C11").Value = WorksheetFunction.CountIfs(released, ctrLowAppReleased, employed, ctrLowAppEmployed, studentsApplications, ">0", studentsApplications, "<=" & lowApp, studentsInterviews, 0, studentsTotalView, ">0")
    'High applications'
    summarySheet.Range("C12").Value = WorksheetFunction.CountIfs(released, ctrHihghAppReleased, employed, ctrHihghAppEmployed, studentsApplications, ">" & highApp, studentsInterviews, 0)
    'High interviews
    summarySheet.Range("C13").Value = WorksheetFunction.CountIfs(released, ctrHighIntervReleased, employed, ctrHighIntervEmployed, studentsInterviews, ">" & highInterview)
    'Total'
    summarySheet.Range("C14").Value = WorksheetFunction.Count(studentID)

    'Ref. Employers'
    'Cancelled'
    summarySheet.Range("F7").Value = WorksheetFunction.CountIfs(status, "Cancelled", employersInterviews, ctrCancelledInterv)
    'No interviews'
    summarySheet.Range("F8").Value = WorksheetFunction.CountIfs(status, "<>" & "Cancelled", appDeliveryMethod, "<>" & "Through Employer Website", sourceOfPosting, "<>" & "Student Developed", employersInterviews, ctrNoIntervCount)
    'No applications'
    summarySheet.Range("F9").Value = WorksheetFunction.CountIfs(status, "<>" & "Cancelled", sourceOfPosting, "<>" & "Student Developed", employersAppCount, ctrJobsNoAppCount)
    'Total postings'
    summarySheet.Range("F10").Value = WorksheetFunction.Count(jobID)
    'Total positions'
    summarySheet.Range("F11").Value = WorksheetFunction.Sum(positions)
    'END - Update summary tables

'END macro
End Sub

'This function will loop the array of column names
Function inArray(theArray, theValue)
    inArray = False
    For i = LBound(theArray) To UBound(theArray)
        If theArray(i) = theValue Then
            inArray = True
            Exit For
        End If
    Next
End Function

'This function converts a column index number to a letter'
'Use row 2! First row only has 2 columns from HireAC (report name and counts)
Function ColumnLetter(ColumnNumber As Integer) As String
ColumnLetter = Split(Cells(2, ColumnNumber).Address(True, False), "$")(0)
End Function
