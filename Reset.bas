Sub StartOver()
'
' StartOver Macro
' Delete students and jobs tables; Reset summary
'

'
    Sheets("Students").Select
    Cells.Select
    Selection.ClearContents
    Selection.Clear
    Range("A1").Select
    Sheets("Jobs").Select
    Cells.Select
    Selection.ClearContents
    Selection.Clear
    Range("A1").Select
    Sheets("Summary").Select
    Range("C7").Select
    ActiveCell.FormulaR1C1 = "0"
    Range("C8").Select
    ActiveCell.FormulaR1C1 = "0"
    Range("C9").Select
    ActiveCell.FormulaR1C1 = "0"
    Range("C10").Select
    ActiveCell.FormulaR1C1 = "0"
    Range("C11").Select
    ActiveCell.FormulaR1C1 = "0"
    Range("C12").Select
    ActiveCell.FormulaR1C1 = "0"
    Range("C13").Select
    ActiveCell.FormulaR1C1 = "0"
    Range("C14").Select
    ActiveCell.FormulaR1C1 = "0"
    Range("F7").Select
    ActiveCell.FormulaR1C1 = "0"
    Range("F8").Select
    ActiveCell.FormulaR1C1 = "0"
    Range("F9").Select
    ActiveCell.FormulaR1C1 = "0"
    Range("F10").Select
    ActiveCell.FormulaR1C1 = "0"
    Range("F11").Select
    ActiveCell.FormulaR1C1 = "0"
    
    Range("C18").Select
    ActiveCell.FormulaR1C1 = "yes"
    Range("C19").Select
    ActiveCell.FormulaR1C1 = "no"
    Range("C20").Select
    ActiveCell.FormulaR1C1 = "no"
    Range("C21").Select
    ActiveCell.FormulaR1C1 = "no"
    Range("C22").Select
    ActiveCell.FormulaR1C1 = "no"
    Range("C23").Select
    ActiveCell.FormulaR1C1 = "no"
    Range("C24").Select
    ActiveCell.FormulaR1C1 = "no"
    
    Range("D19").Select
    ActiveCell.FormulaR1C1 = "no"
    Range("D20").Select
    ActiveCell.FormulaR1C1 = "yes"
    Range("D21").Select
    ActiveCell.FormulaR1C1 = "yes"
    Range("D22").Select
    ActiveCell.FormulaR1C1 = "yes"
    Range("D23").Select
    ActiveCell.FormulaR1C1 = "yes"
    Range("D24").Select
    ActiveCell.FormulaR1C1 = "yes"
    
    Range("E20").Select
    ActiveCell.FormulaR1C1 = "0"
    
    Range("F21").Select
    ActiveCell.FormulaR1C1 = "0"
    
    Range("G20").Select
    ActiveCell.FormulaR1C1 = "0"
    Range("G22").Select
    ActiveCell.FormulaR1C1 = "0"
    Range("G23").Select
    ActiveCell.FormulaR1C1 = "0"
    
    Range("F22").Select
    ActiveCell.Formula = "=I7&""%"""
    Range("F23").Select
    ActiveCell.Formula = "=I8&""%"""
    Range("G24").Select
    ActiveCell.Formula = "=I9"
    

    Range("G26").Select
    ActiveCell.Formula = "0"
    Range("F28").Select
    ActiveCell.Formula = "0"
    Range("G27").Select
    ActiveCell.Formula = "0"
        
    Range("I7").Select
End Sub

