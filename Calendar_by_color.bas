'Christian van Langendonck

Sub Color_code()
'
' Color_code Macro
' Add color code to table rows
'

'
'Create worksheet variables'
Dim Calendar As Worksheet

'Set worksheet variables'
Set Calendar = ActiveSheet

'Create reference variables for last row, last column and table full range'
Dim CalendarLastRow As Long
Dim CalendarLastColumn As Integer
Dim CalendarFullRange As Range

'Find last row
CalendarLastRow = Calendar.Cells(Calendar.Rows.Count, "A").End(xlUp).Row

'Find last Column
CalendarLastColumn = Calendar.Cells(8, Calendar.Columns.Count).End(xlToLeft).Column

'Set the full range for actions'
Set CalendarFullRange = Calendar.Range(Calendar.Cells(9, 1), Calendar.Cells(CalendarLastRow, CalendarLastColumn))

'Option - Remove all colors
If Calendar.Range("H3").Value = "N" And Calendar.Range("H4").Value = "N" Then
    CalendarFullRange.FormatConditions.Delete

'Option - apply colors by term type
ElseIf Calendar.Range("H4").Value = "Y" And Calendar.Range("H3").Value = "N" Then
    CalendarFullRange.FormatConditions.Delete
' GREEN = prep
    CalendarFullRange.FormatConditions.Add Type:=xlExpression, Formula1:="=$E9=""Prep"""
    CalendarFullRange.FormatConditions(CalendarFullRange.FormatConditions.Count).SetFirstPriority
    With CalendarFullRange.FormatConditions(1).Interior
        .PatternColorIndex = xlAutomatic
        .ThemeColor = xlThemeColorAccent6
        .TintAndShade = 0.599963377788629
    End With
    CalendarFullRange.FormatConditions(1).StopIfTrue = False
   
' ORANGE = search
    CalendarFullRange.FormatConditions.Add Type:=xlExpression, Formula1:="=$E9=""Search"""
    CalendarFullRange.FormatConditions(CalendarFullRange.FormatConditions.Count).SetFirstPriority
    With CalendarFullRange.FormatConditions(1).Interior
        .PatternColorIndex = xlAutomatic
        .ThemeColor = xlThemeColorAccent4
        .TintAndShade = 0.599963377788629
    End With
    CalendarFullRange.FormatConditions(1).StopIfTrue = False
   
' BLUE = work
    CalendarFullRange.FormatConditions.Add Type:=xlExpression, Formula1:="=$E9=""Work"""
    CalendarFullRange.FormatConditions(CalendarFullRange.FormatConditions.Count).SetFirstPriority
    With CalendarFullRange.FormatConditions(1).Interior
        .PatternColorIndex = xlAutomatic
        .ThemeColor = xlThemeColorAccent5
        .TintAndShade = 0.599963377788629
    End With
    CalendarFullRange.FormatConditions(1).StopIfTrue = False

'Option - apply colors by calendar type
ElseIf Calendar.Range("H3").Value = "Y" And Calendar.Range("H4").Value = "N" Then
    CalendarFullRange.FormatConditions.Delete
' GREEN = CSA
    CalendarFullRange.FormatConditions.Add Type:=xlExpression, Formula1:="=$A9=""CSA"""
    CalendarFullRange.FormatConditions(CalendarFullRange.FormatConditions.Count).SetFirstPriority
    With CalendarFullRange.FormatConditions(1).Interior
        .PatternColorIndex = xlAutomatic
        .ThemeColor = xlThemeColorAccent6
        .TintAndShade = 0.599963377788629
    End With
    CalendarFullRange.FormatConditions(1).StopIfTrue = False
   
' ORANGE = RO
    CalendarFullRange.FormatConditions.Add Type:=xlExpression, Formula1:="=$A9=""RO"""
    CalendarFullRange.FormatConditions(CalendarFullRange.FormatConditions.Count).SetFirstPriority
    With CalendarFullRange.FormatConditions(1).Interior
        .PatternColorIndex = xlAutomatic
        .ThemeColor = xlThemeColorAccent4
        .TintAndShade = 0.599963377788629
    End With
    CalendarFullRange.FormatConditions(1).StopIfTrue = False
   
' RED = OP
    CalendarFullRange.FormatConditions.Add Type:=xlExpression, Formula1:="=$A9=""OP"""
    CalendarFullRange.FormatConditions(CalendarFullRange.FormatConditions.Count).SetFirstPriority
    With CalendarFullRange.FormatConditions(1).Interior
        .PatternColorIndex = xlAutomatic
        .ThemeColor = xlThemeColorAccent2
        .TintAndShade = 0.599963377788629
    End With
    CalendarFullRange.FormatConditions(1).StopIfTrue = False
    
    ' BLUE = Comm
    CalendarFullRange.FormatConditions.Add Type:=xlExpression, Formula1:="=$A9=""Comm"""
    CalendarFullRange.FormatConditions(CalendarFullRange.FormatConditions.Count).SetFirstPriority
    With CalendarFullRange.FormatConditions(1).Interior
        .PatternColorIndex = xlAutomatic
        .ThemeColor = xlThemeColorAccent5
        .TintAndShade = 0.599963377788629
    End With
    CalendarFullRange.FormatConditions(1).StopIfTrue = False
    
    ' GREY = AC
    CalendarFullRange.FormatConditions.Add Type:=xlExpression, Formula1:="=$A9=""AC"""
    CalendarFullRange.FormatConditions(CalendarFullRange.FormatConditions.Count).SetFirstPriority
    With CalendarFullRange.FormatConditions(1).Interior
        .PatternColorIndex = xlAutomatic
        .ColorIndex = 48
        '.TintAndShade = 0.599963377788629
    End With
    CalendarFullRange.FormatConditions(1).StopIfTrue = False
   
' Do not allow multiple Y
Else
    Range("H3").Select
    ActiveCell.FormulaR1C1 = "N"
    Range("H4").Select
    ActiveCell.FormulaR1C1 = "N"
    MsgBox "Select only one criteria to highligh"
End If
    
    
    
End Sub

