'Christian van Langendonck
'Macro to create events in a Calendar spreadsheet
'Add new event to calendar table

Sub new_event()


'Create worksheet variables'
Dim Calendar As Worksheet

'Set worksheet variables'
Set Calendar = ActiveSheet

'Create reference variables for last row, last column and table full range'
Dim CalendarLastRow As Long
Dim CalendarLastColumn As Integer
Dim CalendarFullRange As Range
Dim DateRange As Range
Dim newValuesColumn As Integer
Dim termStartDate As Range
Dim newEventDate As Range
Dim newTerm As Range
listOfTerms = Array("current", "next", "previous")

'Find last row
CalendarLastRow = Calendar.Cells(Calendar.Rows.Count, "B").End(xlUp).Row

'Find last Column
CalendarLastColumn = Calendar.Cells(9, Calendar.Columns.Count).End(xlToLeft).Column

'Set the full range for actions'
Set CalendarFullRange = Calendar.Range(Calendar.Cells(9, 1), Calendar.Cells(CalendarLastRow, CalendarLastColumn))

'Set Date Range'
Set DateRange = Calendar.Range(Calendar.Cells(9, 2), Calendar.Cells(CalendarLastRow, 2))
newValuesColumn = 6

'Set term start date and new event date
Set termStartDate = Calendar.Range("B7")
Set newEventDate = Calendar.Range("F3")
Set newTerm = Calendar.Range("F6")

'Holidays - Create an array with dates in AC calendar
Dim Holidays(7) As Variant
Dim i As Integer
Dim n As Integer
n = 0
With Calendar
    For i = 9 To CalendarLastRow
        If .Cells(i, 1) = "AC" Then
            Holidays(n) = .Cells(i, 2)
            n = n + 1
        End If
    Next i
End With

'Date input validation
If IsDate(termStartDate) And IsDate(newEventDate) Then
    If isHoliday(newEventDate, Holidays) Then
        MsgBox "Selected date is a holiday - College closed. Please, adjust using weekday control (suggestion, add +1 in column M)"
    End If
    Dim newRow As Long
    newRow = CalendarLastRow + 1
    If newTerm = "current" Then
        Calendar.Range("F" & newRow).Formula = "=IF(MONTH($B$7)=1, ""Winter"", IF(MONTH($B$7)=5, ""Summer"", ""Fall""))"
    ElseIf newTerm = "next" Then
        Calendar.Range("F" & newRow).Formula = "=IF(MONTH($B$7)=1, ""Summer"", IF(MONTH($B$7)=5, ""Fall"", ""Winter""))"
    ElseIf newTerm = "previous" Then
        Calendar.Range("F" & newRow).Formula = "=IF(MONTH($B$7)=1, ""Fall"", IF(MONTH($B$7)=5, ""Winter"", ""Summer""))"
    Else
        MsgBox "Considering the event date, the target term of the event must be current, next or previous."
        End
    End If
'create event
    Dim newWeek As Integer
    Dim newWeekday As Integer
    newWeek = Calendar.Cells(4, 6).Value
    newWeekday = Calendar.Cells(5, 6).Value
    Calendar.Range("C" & newRow).Value = newWeek
    Calendar.Range("M" & newRow).Value = newWeekday
    If Month(termStartDate) = 9 And (newWeek = 1) And (newWeekday < 4) Then
        Calendar.Range("M" & newRow).Value = newWeekday - 1
        MsgBox "Event in the first week of September. WEEKDAY reduced by one to fit all semesters"
    End If
    Calendar.Range("B" & newRow).Formula = "=IF(AND(MONTH($B$7)=9,C" & newRow & "=1,M" & newRow & "<4),DATE(YEAR($B$7),MONTH($B$7),DAY($B$7)+((C" & newRow & "-1)*7)+M" & newRow & "+1),IF(C" & newRow & "<0, DATE(YEAR($B$7),MONTH($B$7),DAY($B$7)+(C" & newRow & "*7)+M" & newRow & "), DATE(YEAR($B$7),MONTH($B$7),DAY($B$7)+((C" & newRow & "-1)*7)+M" & newRow & ")))"
    Calendar.Range("A" & newRow).Select
    MsgBox "New row created - check the last row"
Else
    MsgBox "Chek Term Start Date and Week Calculator Date"
End If

End Sub

'Function to check if a date is a holiday
Function isHoliday(dateToCheck As Range, Holidays As Variant) As Boolean
    Dim toReturn As Boolean
    toReturn = False
    Dim i As Integer
    For i = 0 To 6
        If dateToCheck = Holidays(i) Then
        toReturn = True
        Exit For
        End If
    Next i
    isHoliday = toReturn
End Function

